# Programmierung eines Automaten zum Spiel Vier Gewinnt

## Seminararbeit
Der aktuelle Stand der Seminararbeit liegt in [src/docs/tex](src/docs/tex) und wird über die CI-Pipeline als 4gewinnt.pdf 
[[Download](https://gitlab.com/cthees/4gewinnt/-/jobs/artifacts/dev/raw/build/4gewinnt.pdf?job=build)]
[[Browse](https://gitlab.com/cthees/4gewinnt/-/jobs/artifacts/dev/file/build/4gewinnt.pdf?job=build)]
erstellt.
