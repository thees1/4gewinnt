package dev.thees.connect4;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
public class Spielbrett {
	
	private int[][] felder = new int[7][6];
	private int[] zaehler = new int[7];

	private int[] ablauf = new int[42];
	private int rundenzahl;
	private int anzahl3=0;
	private int anzahl2=0;
	private int anzahl3Gegner=0;
	private int anzahl2Gegner=0;

	Spielbrett(Spielbrett p) {
		if(p!=null) {
		for(int i=0; i<7; i++) {
			for(int j= 0; j<6; j++) {
				felder[i][j]=p.felder[i][j];
			}
		}
		
		for(int i =0; i<7; i++) {
			zaehler[i]= p.zaehler[i];
		}
		
		for(int i =0; i<7; i++) {
			ablauf[i]= p.ablauf[i];
		}
		rundenzahl = p.rundenzahl;
		}
		else {
			resetZaehler();
			for(int i = 0; i<42; i++) {
				ablauf[i]=-1;
			}
		}

	}
	
	public int[][] getFelder(){
		return felder;
	}

	public void resetZaehler() {
		for (int i = 0; i < 7; i++) {
			zaehler[i] = -1;
		}
		rundenzahl = 0;
		for(int i=0; i<7; i++) {
			for(int j= 0; j<6; j++) {
				felder[i][j]=0;
			}
		}
	}
	
	public boolean istSpalteVoll(int Spalte) {
		if(zaehler[Spalte]==5) {
			return true;
		}
		return false;
	}

	public boolean istBeendet() {
		int anzahlBeendet = 0;
		for (int i = 0; i < 7; i++) {
			if (zaehler[i] == 5) {
				anzahlBeendet++;
			}
		}
		if (anzahlBeendet == 7 || checkWin(true) || checkWin(false)) {
			if(anzahlBeendet==7) {
			System.out.println("alles voll");
			}
			return true;
		}
		return false;
	}

	public int getZeileZuSpalte(int spalte, boolean spieler) {

		if (zaehler[spalte] < 5) {
			zaehler[spalte]++;
			if (spieler) {
				felder[spalte][zaehler[spalte]] = 1;
			} else {
				felder[spalte][zaehler[spalte]] = -1;
			}

			ablauf[rundenzahl] = spalte;
			rundenzahl++;

			return zaehler[spalte];
		} else {
			return -1;
		}
	}

	public void export(File f) {
		System.out.println("exportieren");
		File file = f;
		
			
			try {
				FileWriter fw = new FileWriter(file, false);
				for(int i = 0; i<rundenzahl; i++) {
						fw.write((char) (48+ablauf[i]));
						System.out.print(ablauf[i]);
				}
				fw.flush();
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

	}

	public boolean[] getfreieSpalten() {
		boolean[] freieSpalten = new boolean[7];
		for (int i = 0; i < 7; i++) {
			if (zaehler[i] != 5) {
				freieSpalten[i] = true;
			}
		}
		return freieSpalten;
	}

	public boolean checkWin(boolean farbe) {
		int check;
		if (farbe) {
			check = 1;
		} else {
			check = -1;
		}

		// vertikal
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < 3; j++) {
				if (felder[i][j] == check && felder[i][j + 1] == check && check == felder[i][j + 2] && check == felder[i][j + 3]) {
					return true;
				}
			}
		}

		// horinzontal
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 6; j++) {
				if (felder[i][j] == check && check == felder[i + 1][j] && check == felder[i + 2][j] && check == felder[i + 3][j]) {
					return true;
				}
			}
		}

		// diagonal rechts
		for (int i = 3; i < 7; i++) {
			for (int j = 0; j < 3; j++) {
				if (felder[i][j] == check && check == felder[i - 1][j + 1] && check == felder[i - 2][j + 2] && check == felder[i - 3][j + 3]) {
					return true;
				}
			}
		}

		// diagonal links
		for (int i = 6; i > 2; i--) {
			for (int j = 5; j > 2; j--) {
				if (felder[i][j] == check && check == felder[i - 1][j - 1] && check == felder[i - 2][j - 2] && check == felder[i - 3][j - 3]) {
					return true;
				}
			}
		}
		return false;

	}

	public int bewerten(boolean farbe) {
		int wert = 0;
		int automat;
		int gegner;
		
		if(farbe) {
			automat = 1;
			gegner = -1;
		}else {
			automat = -1;
			gegner = 1;
		}
		
		countVertikal(automat, gegner);
		countHorizontal(automat, gegner);
		countDiagonalLinks(automat, gegner);
		countDiagonalRechts(automat, gegner);
		
		wert= 4*anzahl3 + 3*anzahl2-(4*anzahl3Gegner + 3*anzahl2Gegner);
		
		return wert;
	}
	
	public void countVertikal(int a, int g) {
		int leer;
		int besetzt;
		int besetztGegner;
		int automat = a;
		int gegner = g;
		
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < 3; j++) {
				leer = 0;
				besetzt = 0;
				besetztGegner=0;
				for(int z = 0; z<4; z++) {
					if(felder[i][j+z]==automat){
						besetzt++;
					}
					if(felder[i][j+z]==gegner){
						besetztGegner++;
					}else if(felder[i][j+z]==0){
						leer++;
					}
				}
				if(leer==1 && besetzt== 3) {
					anzahl3++;
				}
				if(leer == 2 && besetzt == 2) {
					anzahl2++;
				}
				
				if(leer==1 && besetztGegner== 3) {
					anzahl3Gegner++;
				}
				if(leer == 2 && besetztGegner == 2) {
					anzahl2Gegner++;
				}
			}
		}
	}
	
	public void countHorizontal(int a, int g) {
		int leer;
		int besetzt;
		int besetztGegner;
		int automat = a;
		int gegner = g;
		
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 6; j++) {
				leer = 0;
				besetzt = 0;
				besetztGegner=0;
				for(int z = 0; z<4; z++) {
					if(felder[i+z][j]==automat){
						besetzt++;
					}
					if(felder[i+z][j]==gegner){
						besetztGegner++;
					}else if(felder[i+z][j]==0){
						leer++;
					}
				}
				if(leer==1 && besetzt== 3) {
					anzahl3++;
				}
				if(leer == 2 && besetzt == 2) {
					anzahl2++;
				}
				if(leer==1 && besetztGegner== 3) {
					anzahl3Gegner++;
				}
				if(leer == 2 && besetztGegner == 2) {
					anzahl2Gegner++;
				}
			}
		}

		
	}
	
public void countDiagonalRechts(int a, int g) {
	int leer;
	int besetzt;
	int besetztGegner;
	int automat = a;
	int gegner = g;
	
	for (int i = 3; i < 7; i++) {
		for (int j = 0; j < 3; j++) {
			leer = 0;
			besetzt = 0;
			besetztGegner=0;
			for(int z = 0; z<4; z++) {
				if(felder[i-z][j+z]==automat){
					besetzt++;
				}
				if(felder[i-z][j+z]==gegner){
					besetztGegner++;
				}else if(felder[i-z][j+z]==0){
					leer++;
				}
			}
			if(leer==1 && besetzt== 3) {
				anzahl3++;
			}
			if(leer == 2 && besetzt == 2) {
				anzahl2++;
			}
			
			if(leer==1 && besetztGegner== 3) {
				anzahl3Gegner++;
			}
			if(leer == 2 && besetztGegner == 2) {
				anzahl2Gegner++;
			}
		}
	}
		
	}

public void countDiagonalLinks(int a, int g) {
	int leer;
	int besetzt;
	int besetztGegner;
	int automat = a;
	int gegner = g;
	
	for (int i = 6; i > 2; i--) {
		for (int j = 5; j > 2; j--) {
			leer = 0;
			besetzt = 0;
			besetztGegner=0;
			for(int z = 0; z<4; z++) {
				if(felder[i-z][j-z]==automat){
					besetzt++;
				}
				if(felder[i-z][j-z]==gegner){
					besetztGegner++;
				}else if(felder[i-z][j-z]==0){
					leer++;
				}
			}
			if(leer==1 && besetzt== 3) {
				anzahl3++;
			}
			if(leer == 2 && besetzt == 2) {
				anzahl2++;
			}
			
			if(leer==1 && besetztGegner== 3) {
				anzahl3Gegner++;
			}
			if(leer == 2 && besetztGegner == 2) {
				anzahl2Gegner++;
			}
		}
	}
	
}
	
}