package dev.thees.connect4;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Controller {

	private boolean mehrspieler;
	private boolean zugRot = false;
	Spielbrett VierGewinnt = new Spielbrett(null);
	Automat automat = new Automat(VierGewinnt);

	public static void main(String[] args) {
		Spielfeld.erzeugeSpielfeld(args);
	}
	
	Controller(){
		setFarbeComputer(false);
		automat.setStartTiefe(2);
	}

	public int getZeile(int spalte) {
		zugRot = !zugRot;
		return VierGewinnt.getZeileZuSpalte(spalte, zugRot);
	}

	public boolean getFarbe() {
		return zugRot;
	}
	
	public void resetFarbe() {
		zugRot = false;
	}

	public boolean getFarbeComputer() {
		return automat.farbe;
	}

	public int getZeileAutomat() {
		return automat.getZeile();
	}

	public int getSpalteAutomat() {
		zugRot = !zugRot;
		automat.berechneZug();
		return automat.getSpalte();
	}

	public void setFarbeComputer(boolean farbe) {
		automat.setFarbe(farbe);
	}

	public void setTiefe(int tiefe) {
		automat.setStartTiefe(tiefe);
	}

	public boolean getMehrspieler() {
		return mehrspieler;
	}
	
	public boolean istSpalteVoll(int spalte) {
		return VierGewinnt.istSpalteVoll(spalte);
	}

	public void setzeMehrspieler(boolean mehrsp) {
		mehrspieler = mehrsp;
	}

	public boolean checkWin() {
		return VierGewinnt.checkWin(zugRot);
	}

	public void export(File file) {
		VierGewinnt.export(file);
	}

	public int[] importieren(File file) {
		File f = file;
		FileReader fr = null;
		try {
			fr = new FileReader(f);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		int length = (int) f.length();
		int[] bufInt = new int[length];
		char[] buf = new char[length];

		try {
			fr.read(buf);
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (int i = 0; i < length; i++) {
			bufInt[i] = Character.getNumericValue(buf[i]);
		}

		return bufInt;
	}
	
	public void reset() {
		VierGewinnt.resetZaehler();
	}

	public int[] getVorgabe(File file) {

		reset();
		return importieren(file);
	}

}
