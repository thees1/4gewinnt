package dev.thees.connect4;

public class Automat {

	Spielbrett VierGewinnt;
	boolean farbe;
	private int besteSpalte;
	private int startTiefe;

	Automat(Spielbrett mainBrett) {
		VierGewinnt = mainBrett;
	}

	public void setFarbe(boolean f) {
		farbe = f;
	}

	public void setStartTiefe(int tiefe) {
		startTiefe = tiefe;
	}

	public int getSpalte() {
		return besteSpalte;
	}

	public int getZeile() {
		return VierGewinnt.getZeileZuSpalte(besteSpalte, farbe);
	}

	public void berechneZug() {
		minimax(VierGewinnt, startTiefe, farbe);
	}

	public int minimax(Spielbrett s, int tiefe, boolean spieler) {

		boolean[] freieSpalten = new boolean[7];
		freieSpalten = s.getfreieSpalten();

		if (tiefe == 0 || s.istBeendet()) {
			if (s.checkWin(farbe)) {
				return 1000000+tiefe;
			}
			if (s.checkWin(!farbe)) {
				return -1000000-tiefe;
			}
			if (tiefe == 0) {
				return s.bewerten(spieler);
			} else {
				return 0;
			}
		}
		
		if (spieler == farbe) {
			int wert = Integer.MIN_VALUE;
			for (int i = 0; i < 7; i++) {
				if (freieSpalten[i]) {
					Spielbrett kopie = new Spielbrett(s);
					kopie.getZeileZuSpalte(i, spieler);
					int neuerWert = minimax(kopie, tiefe - 1, !spieler);
					if(tiefe == startTiefe) {
					}
					if (neuerWert > wert) {
						wert = neuerWert;
						if (tiefe == startTiefe) {
							besteSpalte = i;
						}
					}
				}
			}
			return wert;
		}

		if (spieler != farbe) {
			int wert = Integer.MAX_VALUE;
			for (int i = 0; i < 7; i++) {
				if (freieSpalten[i]) {
					Spielbrett kopie = new Spielbrett(s);
					kopie.getZeileZuSpalte(i, spieler);
					int neuerWert = minimax(kopie, tiefe - 1, !spieler);
					if(tiefe == startTiefe-1) {
					}
					if (neuerWert < wert) {
						wert = neuerWert;
					}
				}

			}
			return wert;
		}
		return 0;
	}
}