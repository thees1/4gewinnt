# W-Seminar Checkliste zur Fertigstellung der Seminararbeit

Überprüfen Sie bei der Fertigstellung Ihrer Arbeit folgende Aspekte: 

##	Gliederung und äußere Form 
Habe ich die Vorgaben zur formalen Gestaltung eingehalten?

* * [ ] Schriftgröße
* * [ ] Ränder 
* * [ ] Zeilenabstand
* * [ ] Deckblatt
* * [ ] Inhaltsverzeichnis
* * [ ] Literaturverzeichnis
* * [ ] Abbildungsverzeichnis
* * [ ] Erklärung am Schluss 
* * [ ] Abbildungen

* [ ] Entsprechen die Formulierungen im Inhaltsverzeichnis genau den Überschriften in der Arbeit? 	
* [ ] Ist das Gliederungsschema korrekt und einheitlich?	
* [ ] Entsprechen sich Nummerierung der Kapitel im Inhaltsverzeichnis und in der Ausarbeitung?	
* [ ] Ist die Seitennummerierung korrekt (Deckblatt und Inhaltsverzeichnis ohne Nummerierung)?	

## Inhaltliche Ausführung
* [ ] Wird im Verlauf der Einleitung auf die zentrale(n) Fragestellung(en) meiner Arbeit hingewiesen und der Leser zum Hauptteil hingeführt?	
* [ ] Erläutere ich meine Vorgehensweise und die angewandten Arbeitsschritte und Methoden nachvollziehbar?	
* [ ] Halte ich einen „roten Faden“ ein?	
* [ ] Entsprechen die Kapitel von ihrem Umfang her ihrer Wichtigkeit?	
* [ ] Unterstützen meine Visualisierungen (Diagramme, Bilder, Fotos etc.) das Gesagte oder lenken sie eher davon ab?	
* [ ] Gibt es Gedankensprünge oder Widersprüchlichkeiten?	
* [ ] Runden meine Zusammenfassung und der Schluss die Arbeit sinnvoll ab?	

##  Sprachliche Ausführung
Habe ich meine Arbeit  vor allem auf…
* * [ ] Rechtschreibfehler
* * [ ] Satzzeichenfehler
* * [ ] Tempusfehler
* * [ ] Satzbau- und Grammatikfehler

…überprüft?	
	
* [ ] Finden sich unnötige Wiederholungen?	
* [ ] Habe ich die korrekte Fachsprache verwendet und Fachbegriffe ausreichend erklärt?	

## Zitate, Quellen und Literatur- und Abbildungsverzeichnis
* [ ] Habe ich die Originaltexte genau zitiert und evtl. Änderungen klar ausgewiesen?	
* [ ] Sind alle Zitate als solche markiert und mit entsprechenden Quellenangaben versehen?	
* [ ] Sind meine Zitate zu lang oder zu ungenau?	
* [ ] Sind die Quellenangaben vollständig und in der gesamten Arbeit einheitlich formuliert?	
* [ ] Wurden alle Quellen auch ins Literaturverzeichnis aufgenommen?	
* [ ] Ist das Literaturverzeichnis korrekt geordnet?	
* [ ] Habe ich alle Internetquellen richtig dokumentiert und mit Aufrufdatum versehen?	
* [ ] Habe ich den Bildnachweis im Abbildungsverzeichnis passend zu den Abbildungen korrekt angegeben?	
