Zur Visualisierung und Klärung von Sachverhalten bietet es sich häufig an, Diagramme oder Übersichten aus vorhandenen Publikationen zu verwenden. Auch aussagekräftige Bilder können einzelne Arbeitsergebnisse und Fragestellungen sinnvoll veranschaulichen.

Zu bedenken sind dabei jedoch folgende Fragen:
* [ ] passt die Abbildung zu meinem Thema? Werden zentrale Aspekte meiner Fragestellung behandelt oder thematisiert die Abbildung eher Randbereiche?
* [ ] Nehme ich in meinen Ausführungen sinnvoll und genügend Bezug auf die Abbildung?
* [ ] Dient die Abbildung als Ausgangspunkt, als Veranschaulichung, als Einzelergebnis oder als Zusammenfassung meiner Thesen? Ist sie an der richtigen Stelle eingefügt worden?
* [ ] Sind die zugrunde gelegten Bezeichnungen der Abbildungen eindeutig oder bedarf es hier weiterer Erklärungen?
* [ ] Ist die Nummerierung der Abbildung korrekt und wird diese auch im Text korrekt verwendet?
* [ ] Besitzt die Abbildung eine aussagekräftige und angemessene Bildunterschrift?
* [ ] Findet sich eine eindeutige Quellenangabe? Habe ich sie korrekt übernommen?
* [ ] Wer hat die Publikation in Auftrag gegeben? Mit welcher Absicht und für welche Zielgruppe wurde sie erstellt (z. B. ist die Statistik einer Pharmakette wahrscheinlich anders ausgerichtet als die eines Ökoverbandes)?
* [ ] Wann genau wurde die Untersuchung durchgeführt? Ist sie vielleicht schon veraltet?
* [ ] Welche Fragen kann die Statistik nicht beantworten? Gibt es ausgesparte Bereiche?
* [ ] Was fehlt in der Darstellung?

In jedem Fall sollten Sie stets genau überlegen, ob die gewählte Visualisierung das 
